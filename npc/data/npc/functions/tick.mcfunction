# Functions
function npc:interaction/villager_monk/giveamulet
function npc:interaction/villager_unknown/particles
function npc:interaction/villager_unknown/waves/index
function npc:scores/stone
function npc:interaction/villager_summoner/summons/index

function npc:interaction/villager_forge/tick
function npc:interaction/villager_monk/tick
function npc:interaction/villager_unknown/tick
function npc:interaction/villager_armory/tick
function npc:summon/tick

# Tick count
scoreboard players add currentTick globals 1
execute if score currentTick globals matches 20 run scoreboard players set currentTick globals 0

scoreboard players enable @a cmdTriggerForge
scoreboard players enable @a cmdTriggerMonk
scoreboard players enable @a cmdTriggerUnknown
scoreboard players enable @a cmdTriggerArmory