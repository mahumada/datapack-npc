# Update on mined
execute if score currentTick globals matches 1 run scoreboard players add @a[scores={player_mined_stone=1..}] player_mined_stone_count 1
execute if score currentTick globals matches 1 run scoreboard players add @a[scores={player_mined_deepslate=1..}] player_mined_stone_count 1

# Trigger Give Item
execute as @a[scores={player_mined_stone_count=15..}] run function npc:giveitem/titanitefragment
scoreboard players reset @a[scores={player_mined_stone_count=15..}] player_mined_stone_count

# Reset triggers
scoreboard players reset @a player_mined_stone
scoreboard players reset @a player_mined_deepslate