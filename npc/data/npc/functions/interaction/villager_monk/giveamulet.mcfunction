# Level 1
execute at @e[type=snowball,nbt={Item:{tag:{Tags:["temple_amulet","level_1"]}}}] run execute as @p run effect give @s[scores={temple_amulet_cooldown=0}] regeneration 5 0 false
execute at @e[type=snowball,nbt={Item:{tag:{Tags:["temple_amulet","level_1"]}}}] run scoreboard players operation @p cooldownDisplay = @p temple_amulet_cooldown
execute at @e[type=snowball,nbt={Item:{tag:{Tags:["temple_amulet","level_1"]}}}] run scoreboard players operation @p cooldownDisplay /= tickToSecond globals 
execute at @e[type=snowball,nbt={Item:{tag:{Tags:["temple_amulet","level_1"]}}}] run execute as @p run title @s[scores={temple_amulet_cooldown=1..}] actionbar [{"text":"En Cooldown - ","color":"dark_red","bold":true},{"score":{"objective":"cooldownDisplay","name":"@s"}},{"text":"s","color":"dark_red","bold":true}]
execute at @e[type=snowball,nbt={Item:{tag:{Tags:["temple_amulet","level_1"]}}}] run execute as @p run scoreboard players set @s[scores={temple_amulet_cooldown=0}] temple_amulet_cooldown 100
execute at @e[type=snowball,nbt={Item:{tag:{Tags:["temple_amulet","level_1"]}}}] run give @p snowball{CustomModelData:2,Tags:["temple_amulet","level_1"],display:{Name:'[{"text":"Amuleto del Templo","italic":false,"color":"white","bold":true}]',Lore:['[{"text":"Nivel 1","italic":false,"color":"gray"},{"text":"","italic":false,"color":"dark_purple"}]','[{"text":"","italic":false,"color":"dark_purple"}]','[{"text":"ITEM ESPECIAL","italic":false,"color":"gold"}]']},Enchantments:[{}],HideFlags:1}
kill @e[type=snowball,nbt={Item:{tag:{Tags:["temple_amulet","level_1"]}}}]

# Level 2
execute at @e[type=snowball,nbt={Item:{tag:{Tags:["temple_amulet","level_2"]}}}] run execute as @p run effect give @s[scores={temple_amulet_cooldown=0}] regeneration 20 0 false
execute at @e[type=snowball,nbt={Item:{tag:{Tags:["temple_amulet","level_2"]}}}] run execute as @p run effect give @s[scores={temple_amulet_cooldown=0}] speed 3 0 false
# execute at @e[type=snowball,nbt={Item:{tag:{Tags:["temple_amulet","level_2"]}}}] run execute at @p[scores={temple_amulet_cooldown=0}] run execute at @a[distance=0.1..5] run summon skeleton ~ ~ ~ {Health:40,Tags:["amulet_skeleton"],Attributes:[{Name:"generic.max_health",Base:40}]}
execute at @e[type=snowball,nbt={Item:{tag:{Tags:["temple_amulet","level_2"]}}}] run execute as @p run execute at @s[scores={temple_amulet_cooldown=0}] run particle soul ~ ~ ~ 2 0 2 0.001 400
execute at @e[type=snowball,nbt={Item:{tag:{Tags:["temple_amulet","level_2"]}}}] run scoreboard players operation @p cooldownDisplay = @p temple_amulet_cooldown
execute at @e[type=snowball,nbt={Item:{tag:{Tags:["temple_amulet","level_2"]}}}] run scoreboard players operation @p cooldownDisplay /= tickToSecond globals 
execute at @e[type=snowball,nbt={Item:{tag:{Tags:["temple_amulet","level_2"]}}}] run execute as @p run title @s[scores={temple_amulet_cooldown=1..}] actionbar [{"text":"En Cooldown - ","color":"dark_red","bold":true},{"score":{"objective":"cooldownDisplay","name":"@s"}},{"text":"s","color":"dark_red","bold":true}]
execute at @e[type=snowball,nbt={Item:{tag:{Tags:["temple_amulet","level_2"]}}}] run execute as @p run scoreboard players set @s[scores={temple_amulet_cooldown=0}] temple_amulet_cooldown 400
execute at @e[type=snowball,nbt={Item:{tag:{Tags:["temple_amulet","level_2"]}}}] run give @p snowball{CustomModelData:2,Tags:["temple_amulet","level_2"],display:{Name:'[{"text":"Amuleto del Templo","italic":false,"color":"white","bold":true}]',Lore:['[{"text":"Nivel 2","italic":false,"color":"gray"},{"text":"","italic":false,"color":"dark_purple"}]','[{"text":"","italic":false,"color":"dark_purple"}]','[{"text":"ITEM ESPECIAL","italic":false,"color":"gold"}]']},Enchantments:[{}],HideFlags:1}
kill @e[type=snowball,nbt={Item:{tag:{Tags:["temple_amulet","level_2"]}}}]

# Cooldown
scoreboard players remove @a[scores={temple_amulet_cooldown=1..}] temple_amulet_cooldown 1