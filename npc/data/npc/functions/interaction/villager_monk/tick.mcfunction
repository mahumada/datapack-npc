execute as @a[scores={cmdTriggerMonk=1}] run function npc:interaction/villager_monk/dialogues/options/1/accept
execute as @a[scores={cmdTriggerMonk=2}] run function npc:interaction/villager_monk/dialogues/options/1/decline
execute as @a[scores={cmdTriggerMonk=3}] run function npc:interaction/villager_monk/dialogues/options/2/accept
execute as @a[scores={cmdTriggerMonk=4}] run function npc:interaction/villager_monk/dialogues/options/2/decline
execute as @a[scores={cmdTriggerMonk=5}] run function npc:interaction/villager_monk/dialogues/options/3/good
execute as @a[scores={cmdTriggerMonk=6}] run function npc:interaction/villager_monk/dialogues/options/3/bad
execute as @a[scores={cmdTriggerMonk=7}] run function npc:interaction/villager_monk/dialogues/options/7/accept
execute as @a[scores={cmdTriggerMonk=8}] run function npc:interaction/villager_monk/dialogues/options/7/decline
scoreboard players reset @a cmdTriggerMonk