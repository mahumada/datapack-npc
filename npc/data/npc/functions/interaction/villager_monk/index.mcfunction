# Reset Trigger
advancement revoke @s from npc:villager_monk

# Dialogues
scoreboard players set @s[tag=!villager_monk_interacted] interaction_villager_monk 0
execute as @s[scores={interaction_villager_monk=7}] run function npc:interaction/villager_monk/dialogues/7
execute as @s[scores={interaction_villager_monk=6}] run function npc:interaction/villager_monk/dialogues/6
execute as @s[scores={interaction_villager_monk=5}] run function npc:interaction/villager_monk/dialogues/5
execute as @s[scores={interaction_villager_monk=4}] run function npc:interaction/villager_monk/dialogues/4
execute as @s[scores={interaction_villager_monk=3}] run function npc:interaction/villager_monk/dialogues/3
execute as @s[scores={interaction_villager_monk=2}] run function npc:interaction/villager_monk/dialogues/2
execute as @s[scores={interaction_villager_monk=1}] run function npc:interaction/villager_monk/dialogues/1
execute as @s[scores={interaction_villager_monk=0}] run function npc:interaction/villager_monk/dialogues/0

# Add Tag
tag @s add villager_monk_interacted