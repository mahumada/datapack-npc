playsound entity.villager.no master @s ~ ~ ~ 1 0.7 0
scoreboard players set @s item_in_inventory_list_check 0
# Check bones
execute as @s store result score @s item_in_inventory_count run clear @s bone 0
scoreboard players add @s[scores={item_in_inventory_count=24..}] item_in_inventory_list_check 1
# Check String
execute as @s store result score @s item_in_inventory_count run clear @s string 0
scoreboard players add @s[scores={item_in_inventory_count=12..}] item_in_inventory_list_check 1
# Check Redstone Powder
execute as @s store result score @s item_in_inventory_count run clear @s redstone 0
scoreboard players add @s[scores={item_in_inventory_count=3..}] item_in_inventory_list_check 1
# Check Diamond
execute as @s store result score @s item_in_inventory_count run clear @s diamond 0
scoreboard players add @s[scores={item_in_inventory_count=1..}] item_in_inventory_list_check 1

# Options
tellraw @s[scores={item_in_inventory_list_check=..3}] ["",{"text":"\n\n\n\n\n"},{"text":"<Rahtra>","bold":true,"underlined":true,"color":"dark_purple"},{"text":"\n"},{"text":"Tu primer paso en el templo es conseguir tu amuleto, para eso debes encontrar los materiales y darmelos para que yo pueda crearlo y bendecirlo...\n","color":"light_purple"},{"text":"Al'Rot!","bold":true,"color":"light_purple"},{"text":"\n\n"},{"text":"MISION:","bold":true,"underlined":true,"color":"gold"},{"text":"\n"},{"text":"Debes dar los siguientes materiales a Rahtra:\n24x Bone\n12x String\n3x Redstone Powder\n1x Diamond\n","color":"gray"}]
tellraw @s[scores={item_in_inventory_list_check=4}] ["",{"text":"\n\n\n\n\n"},{"text":"<Rahtra>","bold":true,"underlined":true,"color":"dark_purple"},{"text":"\n"},{"text":"Veo que tienes los materiales necesarios para craftear tu Amuleto del Templo, este es un objeto especial de los miembros de mi templo que permite reforzar la salud en combate, deseas que lo craftee?","color":"light_purple"},{"text":"\n\n"},{"text":"[ Si ] ","bold":true,"color":"green","clickEvent":{"action":"run_command","value":"/trigger cmdTriggerMonk set 3"}},{"text":" [ No ]","bold":true,"color":"dark_red","clickEvent":{"action":"run_command","value":"/trigger cmdTriggerMonk set 4"}},{"text":"\n\n"},{"text":"ATENCION: Debes tener espacio en el inventario para recibir el amuleto.\n","bold":true,"color":"red"}]