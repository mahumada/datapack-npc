scoreboard players set @s item_in_inventory_list_check 0
# Check bones
execute as @s store result score @s item_in_inventory_count run clear @s bone 0
scoreboard players add @s[scores={item_in_inventory_count=24..}] item_in_inventory_list_check 1
# Check String
execute as @s store result score @s item_in_inventory_count run clear @s string 0
scoreboard players add @s[scores={item_in_inventory_count=12..}] item_in_inventory_list_check 1
# Check Redstone Powder
execute as @s store result score @s item_in_inventory_count run clear @s redstone 0
scoreboard players add @s[scores={item_in_inventory_count=3..}] item_in_inventory_list_check 1
# Check Diamond
execute as @s store result score @s item_in_inventory_count run clear @s diamond 0
scoreboard players add @s[scores={item_in_inventory_count=1..}] item_in_inventory_list_check 1

# Error check
execute as @s[scores={item_in_inventory_list_check=..3}] run function npc:interaction/villager_monk/dialogues/options/common/punish

# Success
scoreboard players set @s[scores={item_in_inventory_list_check=4}] temple_amulet_cooldown 0
scoreboard players set @s[scores={item_in_inventory_list_check=4}] interaction_villager_monk 3
tellraw @s[scores={item_in_inventory_list_check=4}] ["",{"text":"\n\n\n\n\n"},{"text":"<Rahtra>","bold":true,"underlined":true,"color":"dark_purple"},{"text":"\n"},{"text":"Utiliza este gran poder para llevar a este templo a la cima\nAl'Rot!","color":"light_purple"},{"text":"\n "}]
execute at @s[scores={item_in_inventory_list_check=4}] run playsound entity.player.levelup master @s ~ ~ ~ 1 0.7 0
advancement grant @s[scores={item_in_inventory_list_check=4}] only npc:temple/2
give @s[scores={item_in_inventory_list_check=4}] snowball{CustomModelData:2,Tags:["temple_amulet","level_1"],display:{Name:'[{"text":"Amuleto del Templo","italic":false,"color":"white","bold":true}]',Lore:['[{"text":"Nivel 1","italic":false,"color":"gray"},{"text":"","italic":false,"color":"dark_purple"}]','[{"text":"","italic":false,"color":"dark_purple"}]','[{"text":"ITEM ESPECIAL","italic":false,"color":"gold"}]']},Enchantments:[{}],HideFlags:1}
clear @s[scores={item_in_inventory_list_check=4}] bone 24
clear @s[scores={item_in_inventory_list_check=4}] string 12
clear @s[scores={item_in_inventory_list_check=4}] redstone 3
clear @s[scores={item_in_inventory_list_check=4}] diamond 1