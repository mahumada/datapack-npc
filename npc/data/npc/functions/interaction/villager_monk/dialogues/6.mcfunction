# Uncomplete
playsound entity.villager.no master @s[scores={player_killed_cow=..19}] ~ ~ ~ 1 0.6 0
tellraw @s[scores={player_killed_cow=..19}] ["",{"text":"\n\n\n\n\n"},{"text":"<Rahtra>","bold":true,"underlined":true,"color":"dark_purple"},{"text":"\n"},{"text":"Rottram tiene sed de sangre y debemos satisfacerlo, necesito que manches tus manos por el bien de nuestro templo fiel hermano...","color":"light_purple"},{"text":"\n\n"},{"text":"MISION:","bold":true,"underlined":true,"color":"gold"},{"text":"\n"},{"text":"Asesina 20 vacas","bold":true,"italic":true,"color":"dark_red"},{"text":"\n "}]

# Complete
playsound entity.villager.no master @s[scores={player_killed_cow=20..}] ~ ~ ~ 1 1 0
tellraw @s[scores={player_killed_cow=20..}] ["",{"text":"\n\n\n\n\n"},{"text":"<Rahtra>","bold":true,"underlined":true,"color":"dark_purple"},{"text":"\n"},{"text":"Haz conseguido sacrificar tu moral por el bien del templo, muchas gracias fiel seguidor\n","color":"light_purple"},{"text":"Al'Rot!","bold":true,"color":"light_purple"},{"text":"\n "}]
advancement grant @s[scores={player_killed_cow=20..}] only npc:temple/3
scoreboard players set @s[scores={player_killed_cow=20..}] interaction_villager_monk 7
scoreboard players set @s player_killed_skeleton 0