# Item Count
execute as @s store result score @s item_in_inventory_count run clear @s flint{Tags:["titanite_fragment"]} 0
tag @s[scores={item_in_inventory_count=1..}] add payment_ok
# Not Enough
execute as @s[tag=!payment_ok] run tellraw @s ["",{"text":"\n\n\n\n\n"},{"text":"<Ruhk>","bold":true,"underlined":true,"color":"gray"},{"text":"\nNo tienes los fragmentos de titanita suficientes para adquirir esta compra...\n "}]
# Enough
give @s[tag=payment_ok] wooden_shovel{Tags:["forge_shovel","level_1"],display:{Name:'[{"text":"Pala del Guerrero","italic":false,"color":"dark_red","bold":true}]',Lore:['[{"text":"Nivel 1","italic":false,"color":"gray"},{"text":"","italic":false,"color":"dark_purple"}]','[{"text":"","italic":false,"color":"dark_purple"}]','[{"text":"ITEM ESCALABLE","italic":false,"color":"gold"}]']},Unbreakable:1b} 1
clear @s[tag=payment_ok] flint{Tags:["titanite_fragment"]} 1
advancement grant @s only npc:forge/shovel_1