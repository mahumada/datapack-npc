# Reset Trigger
advancement revoke @s from npc:villager_armory

# Dialogues
scoreboard players set @s[tag=!villager_armory_interacted] interaction_villager_armory 0
execute as @s[scores={interaction_villager_armory=1}] run function npc:interaction/villager_armory/dialogues/1
execute as @s[scores={interaction_villager_armory=0}] run function npc:interaction/villager_armory/dialogues/0

# Add Tag
tag @s add villager_armory_interacted