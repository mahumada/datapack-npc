execute as @a[scores={cmdTriggerArmory=1}] run function npc:interaction/villager_armory/items/pickaxe
execute as @a[scores={cmdTriggerArmory=2}] run function npc:interaction/villager_armory/items/axe
execute as @a[scores={cmdTriggerArmory=3}] run function npc:interaction/villager_armory/items/shovel
execute as @a[scores={cmdTriggerArmory=4}] run function npc:interaction/villager_armory/items/sword
scoreboard players reset @a cmdTriggerArmory