playsound minecraft:entity.axolotl.hurt master @s ~ ~ ~ 1 0.5 0
execute as @s store result score @s item_in_inventory_count run clear @s cobbled_deepslate 0
tellraw @s[scores={item_in_inventory_count=..23}] ["",{"text":"\n\n\n\n\n"},{"text":"<Prohl>","bold":true,"underlined":true,"color":"gold"},{"text":"\n"},{"text":"Ya no tienes los materiales... No aproveches mi amabilidad...","color":"yellow"},{"text":"\n "}]
effect give @s[scores={item_in_inventory_count=..23}] poison 60 1 true
tellraw @s[scores={item_in_inventory_count=24..}] ["",{"text":"\n\n\n\n\n"},{"text":"<Prohl>","bold":true,"underlined":true,"color":"gold"},{"text":"\n"},{"text":"Muchas gracias por las muestras... Que comience el analisis!","color":"yellow"},{"text":"\n "}]
clear @s[scores={item_in_inventory_count=24..}] cobbled_deepslate 24
advancement grant @s[scores={item_in_inventory_count=24..}] only npc:lab/1
scoreboard players set @s[scores={item_in_inventory_count=24..}] interaction_villager_lab 2