# Reset Trigger
advancement revoke @s from npc:villager_lab

# Dialogues
scoreboard players set @s[tag=!villager_lab_interacted] interaction_villager_lab 0
execute as @s[scores={interaction_villager_lab=2}] run function npc:interaction/villager_lab/dialogues/2
execute as @s[scores={interaction_villager_lab=1}] run function npc:interaction/villager_lab/dialogues/1
execute as @s[scores={interaction_villager_lab=0}] run function npc:interaction/villager_lab/dialogues/0

# Add Tag
tag @s add villager_lab_interacted