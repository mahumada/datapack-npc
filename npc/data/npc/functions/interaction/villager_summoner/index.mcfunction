# Reset Trigger
advancement revoke @s from npc:villager_summoner

# Dialogues
scoreboard players set @s[tag=!villager_summoner_interacted] interaction_villager_summoner 0
execute as @s[scores={interaction_villager_summoner=1}] run function npc:interaction/villager_summoner/dialogues/1
execute as @s[scores={interaction_villager_summoner=0}] run function npc:interaction/villager_summoner/dialogues/0

# Add Tag
tag @s add villager_summoner_interacted