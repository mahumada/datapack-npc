scoreboard players set @s item_in_inventory_list_check 0
# Check Ender Pearl
execute as @s store result score @s item_in_inventory_count run clear @s ender_pearl 0
scoreboard players add @s[scores={item_in_inventory_count=3..}] item_in_inventory_list_check 1
# Check Spider Eye
execute as @s store result score @s item_in_inventory_count run clear @s spider_eye 0
scoreboard players add @s[scores={item_in_inventory_count=9..}] item_in_inventory_list_check 1
# Check Brown Mushroom
execute as @s store result score @s item_in_inventory_count run clear @s brown_mushroom 0
scoreboard players add @s[scores={item_in_inventory_count=16..}] item_in_inventory_list_check 1
# Check Red Mushroom
execute as @s store result score @s item_in_inventory_count run clear @s red_mushroom 0
scoreboard players add @s[scores={item_in_inventory_count=16..}] item_in_inventory_list_check 1

# Check
tellraw @s[scores={item_in_inventory_list_check=..3}] ["",{"text":"\n\n\n\n\n"},{"text":"<unknown>","bold":true,"underlined":true,"color":"dark_blue"},{"text":"\n"},{"text":"Ya no tienes los materiales, como pretendes que haga el fragmento? No me mientas...","color":"blue"},{"text":"\n "}]
effect give @s[scores={item_in_inventory_list_check=..3}] poison 60 1 false
playsound minecraft:entity.allay.hurt master @s[scores={item_in_inventory_list_check=..3}] ~ ~ ~ 1 0.3 0

# OK
give @s[scores={item_in_inventory_list_check=4}] snowball{CustomModelData:4,Tags:["wave_distort"],display:{Name:'[{"text":"Onda de Distorsion","italic":false,"bold":true,"color":"aqua"}]',Lore:['[{"text":"","italic":false}]','[{"text":"ITEM CONSUMIBLE","italic":false,"color":"gold"}]']},Enchantments:[{}]}
scoreboard players set @s[scores={item_in_inventory_list_check=4}] interaction_villager_unknown 5
advancement grant @s[scores={item_in_inventory_list_check=4}] only npc:strings/4
tellraw @s[scores={item_in_inventory_list_check=4}] ["",{"text":"\n\n\n\n\n"},{"text":"<unknown>","bold":true,"underlined":true,"color":"dark_blue"},{"text":"\n"},{"text":"Encuentra su poder oculto y se el mejor...","color":"blue"},{"text":"\n "}]
scoreboard players set @s[scores={item_in_inventory_list_check=4}] wave_distort_cooldown 0
clear @s[scores={item_in_inventory_list_check=4}] ender_pearl 3
clear @s[scores={item_in_inventory_list_check=4}] spider_eye 9
clear @s[scores={item_in_inventory_list_check=4}] brown_mushroom 12
clear @s[scores={item_in_inventory_list_check=4}] red_mushroom 12