
# Check materials
execute as @s store result score @s item_in_inventory_count run clear @s amethyst_shard 0

tellraw @s[scores={item_in_inventory_count=..7}] ["",{"text":"\n\n\n\n\n"},{"text":"<unknown>","bold":true,"underlined":true,"color":"dark_blue"},{"text":"\n"},{"text":"Ya no tienes los materiales, como pretendes que termine la mesa? No me mientas...","color":"blue"},{"text":"\n "}]
effect give @s[scores={item_in_inventory_count=..7}] poison 60 1 false
playsound minecraft:entity.allay.hurt master @s[scores={item_in_inventory_count=..7}] ~ ~ ~ 1 0.3 0

tellraw @s[scores={item_in_inventory_count=8..}] ["",{"text":"\n\n\n\n\n"},{"text":"<unknown>","bold":true,"underlined":true,"color":"dark_blue"},{"text":"\n"},{"text":"Perfecto, con esto puedo terminarla, muchas gracias\nVuelve aqui y descubriremos todo tipo de ondas","color":"blue"},{"text":"\n "}]
advancement grant @s[scores={item_in_inventory_count=8..}] only npc:strings/1
clear @s[scores={item_in_inventory_count=8..}] amethyst_shard 8
playsound minecraft:entity.allay.hurt master @s[scores={item_in_inventory_count=8..}] ~ ~ ~ 1 0.4 0
scoreboard players set @s[scores={item_in_inventory_count=8..}] interaction_villager_unknown 2