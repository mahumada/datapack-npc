playsound minecraft:entity.allay.hurt master @s ~ ~ ~ 1 0.4 0

scoreboard players set @s item_in_inventory_list_check 0
# Check bones
execute as @s store result score @s item_in_inventory_count run clear @s redstone 0
scoreboard players add @s[scores={item_in_inventory_count=64..}] item_in_inventory_list_check 1
# Check String
execute as @s store result score @s item_in_inventory_count run clear @s raw_copper 0
scoreboard players add @s[scores={item_in_inventory_count=32..}] item_in_inventory_list_check 1

# Check
tellraw @s[scores={item_in_inventory_list_check=..1}] ["",{"text":"\n\n\n\n\n"},{"text":"<unknown>","bold":true,"underlined":true,"color":"dark_blue"},{"text":"\n"},{"text":"Ya no tienes los materiales, como pretendes que haga el fragmento? No me mientas...","color":"blue"},{"text":"\n "}]
effect give @s[scores={item_in_inventory_list_check=..1}] poison 60 1 false
playsound minecraft:entity.allay.hurt master @s[scores={item_in_inventory_list_check=..1}] ~ ~ ~ 1 0.3 0

# OK
give @s[scores={item_in_inventory_list_check=2}] snowball{CustomModelData:4,Tags:["wave_impact"],display:{Name:'[{"text":"Onda de Impacto","italic":false,"bold":true,"color":"aqua"}]',Lore:['[{"text":"","italic":false}]','[{"text":"ITEM CONSUMIBLE","italic":false,"color":"gold"}]']},Enchantments:[{}]}
scoreboard players set @s[scores={item_in_inventory_list_check=2}] interaction_villager_unknown 3
advancement grant @s[scores={item_in_inventory_list_check=2}] only npc:strings/2
tellraw @s[scores={item_in_inventory_list_check=2}] ["",{"text":"\n\n\n\n\n"},{"text":"<unknown>","bold":true,"underlined":true,"color":"dark_blue"},{"text":"\n"},{"text":"Encuentra su poder oculto y se el mejor...","color":"blue"},{"text":"\n "}]
scoreboard players set @s[scores={item_in_inventory_list_check=2}] wave_impact_cooldown 0
clear @s[scores={item_in_inventory_list_check=2}] redstone 64
clear @s[scores={item_in_inventory_list_check=2}] raw_copper 32
scoreboard players set @s[scores={item_in_inventory_list_check=2}] player_killed_enderman 0