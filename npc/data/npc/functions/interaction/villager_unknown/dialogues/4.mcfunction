playsound minecraft:entity.allay.hurt master @s ~ ~ ~ 1 0.4 0

scoreboard players set @s item_in_inventory_list_check 0
# Check Ender Pearl
execute as @s store result score @s item_in_inventory_count run clear @s ender_pearl 0
scoreboard players add @s[scores={item_in_inventory_count=3..}] item_in_inventory_list_check 1
# Check Spider Eye
execute as @s store result score @s item_in_inventory_count run clear @s spider_eye 0
scoreboard players add @s[scores={item_in_inventory_count=9..}] item_in_inventory_list_check 1
# Check Brown Mushroom
execute as @s store result score @s item_in_inventory_count run clear @s brown_mushroom 0
scoreboard players add @s[scores={item_in_inventory_count=16..}] item_in_inventory_list_check 1
# Check Red Mushroom
execute as @s store result score @s item_in_inventory_count run clear @s red_mushroom 0
scoreboard players add @s[scores={item_in_inventory_count=16..}] item_in_inventory_list_check 1

tellraw @s[scores={item_in_inventory_list_check=..3}] ["",{"text":"\n\n\n\n\n"},{"text":"<unknown>","bold":true,"underlined":true,"color":"dark_blue"},{"text":"\n"},{"text":"Tengo una onda armonica disponible para transmitir, necesito que encuentres ciertos materiales para hacerlo posible...","color":"blue"},{"text":"\n\n"},{"text":"MISION:","bold":true,"underlined":true,"color":"gold"},{"text":"\n"},{"text":"Dar los siguientes materiales a unknown:\n3x Ender Pearl\n9x Spider Eye\n12x Brown Mushroom\n12x Red Mushroom","color":"gray"},{"text":"\n "}]
tellraw @s[scores={item_in_inventory_list_check=4}] ["",{"text":"\n\n\n\n\n"},{"text":"<unknown>","bold":true,"underlined":true,"color":"dark_blue"},{"text":"\n"},{"text":"Perfecto, veo que tienes los materiales necesarios. Damelos y te dare el fragmento con el poder de esta onda armonica...","color":"blue"},{"text":"\n\n"},{"text":"[ Dar ] ","bold":true,"color":"green","clickEvent":{"action":"run_command","value":"/trigger cmdTriggerUnknown set 5"}},{"text":" [ Rechazar ]","bold":true,"color":"dark_red","clickEvent":{"action":"run_command","value":"/trigger cmdTriggerUnknown set 6"}},{"text":"\n "}]