playsound minecraft:entity.allay.hurt master @s ~ ~ ~ 1 0.4 0

scoreboard players set @s item_in_inventory_list_check 0
# Check redstone
execute as @s store result score @s item_in_inventory_count run clear @s redstone 0
scoreboard players add @s[scores={item_in_inventory_count=64..}] item_in_inventory_list_check 1
# Check copper
execute as @s store result score @s item_in_inventory_count run clear @s raw_copper 0
scoreboard players add @s[scores={item_in_inventory_count=32..}] item_in_inventory_list_check 1

tellraw @s[scores={item_in_inventory_list_check=..1}] ["",{"text":"\n\n\n\n\n"},{"text":"<unknown>","bold":true,"underlined":true,"color":"dark_blue"},{"text":"\n"},{"text":"Creo que estoy por conseguir la primer onda armonica con nuestra mesa de inferencia, pero voy a necesitar mas energia para poder transferirla... Podrias conseguir los materiales necesarios? A cambio obtendras un fragmento imbuido con esta energia... Su poder podria hacer cosas unicas...","color":"blue"},{"text":"\n\n"},{"text":"MISION:","underlined":true,"color":"gold"},{"text":"\n"},{"text":"Dar los siguientes materiales a unknown:\n64x Redstone Powder\n32x Raw Copper","color":"gray"},{"text":"\n "}]
tellraw @s[scores={item_in_inventory_list_check=2}] ["",{"text":"\n\n\n\n\n"},{"text":"<unknown>","bold":true,"underlined":true,"color":"dark_blue"},{"text":"\n"},{"text":"Has conseguido los materiales? Perfecto, damelos y a cambio te dare tu fragmento...","color":"blue"},{"text":"\n\n"},{"text":"[ Dar ] ","color":"green","clickEvent":{"action":"run_command","value":"/trigger cmdTriggerUnknown set 3"}},{"text":" [ Rechazar ]","color":"dark_red","clickEvent":{"action":"run_command","value":"/trigger cmdTriggerUnknown set 4"}},{"text":"\n\n"},{"text":"ATENCION: Debes tener espacio en el inventario para recibir el fragmento","bold":true,"color":"red"},{"text":"\n "}]