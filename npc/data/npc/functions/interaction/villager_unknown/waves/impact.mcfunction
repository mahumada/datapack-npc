execute at @e[type=snowball,nbt={Item:{tag:{Tags:["wave_impact"]}}}] run execute as @p run execute at @s[scores={wave_impact_cooldown=0}] run effect give @e[distance=0.1..5] instant_damage
execute at @e[type=snowball,nbt={Item:{tag:{Tags:["wave_impact"]}}}] run execute as @p run execute as @s[scores={wave_impact_cooldown=0}] run particle scrape ~ ~ ~ 3 0 3 0.1 2000
execute at @e[type=snowball,nbt={Item:{tag:{Tags:["wave_impact"]}}}] run execute as @p run execute as @s[scores={wave_impact_cooldown=0}] run playsound block.beacon.power_select master @a ~ ~ ~ 1 2 0

execute at @e[type=snowball,nbt={Item:{tag:{Tags:["wave_impact"]}}}] run scoreboard players operation @p cooldownDisplay = @p wave_impact_cooldown
execute at @e[type=snowball,nbt={Item:{tag:{Tags:["wave_impact"]}}}] run scoreboard players operation @p cooldownDisplay /= tickToSecond globals 
execute at @e[type=snowball,nbt={Item:{tag:{Tags:["wave_impact"]}}}] run execute as @p run title @s[scores={wave_impact_cooldown=1..}] actionbar [{"text":"En Cooldown - ","color":"dark_red","bold":true},{"score":{"objective":"cooldownDisplay","name":"@s"}},{"text":"s","color":"dark_red","bold":true}]

execute at @e[type=snowball,nbt={Item:{tag:{Tags:["wave_impact"]}}}] run execute as @p run execute as @s[scores={wave_impact_cooldown=0}] run scoreboard players set @s wave_impact_cooldown 900
execute at @e[type=snowball,nbt={Item:{tag:{Tags:["wave_impact"]}}}] run give @p snowball{CustomModelData:4,Tags:["wave_impact"],display:{Name:'[{"text":"Onda de Impacto","italic":false,"bold":true,"color":"aqua"}]',Lore:['[{"text":"","italic":false}]','[{"text":"ITEM CONSUMIBLE","italic":false,"color":"gold"}]']},Enchantments:[{}]}
scoreboard players remove @a[scores={wave_impact_cooldown=1..}] wave_impact_cooldown 1
kill @e[type=snowball,nbt={Item:{tag:{Tags:["wave_impact"]}}}]