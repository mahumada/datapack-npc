execute at @e[type=snowball,nbt={Item:{tag:{Tags:["wave_distort"]}}}] run execute as @p run execute at @s[scores={wave_distort_cooldown=0}] run effect give @e[distance=0.1..5] nausea 16 1 false
execute at @e[type=snowball,nbt={Item:{tag:{Tags:["wave_distort"]}}}] run execute as @p run execute at @s[scores={wave_distort_cooldown=0}] run effect give @e[distance=0.1..5] wither 16 0 false
execute at @e[type=snowball,nbt={Item:{tag:{Tags:["wave_distort"]}}}] run execute as @p run execute as @s[scores={wave_distort_cooldown=0}] run particle scrape ~ ~ ~ 3 0 3 0.1 2000
execute at @e[type=snowball,nbt={Item:{tag:{Tags:["wave_distort"]}}}] run execute as @p run execute as @s[scores={wave_distort_cooldown=0}] run particle cloud ~ ~3 ~ 3 0 3 0.1 1000
execute at @e[type=snowball,nbt={Item:{tag:{Tags:["wave_distort"]}}}] run execute as @p run execute as @s[scores={wave_distort_cooldown=0}] run playsound entity.enderman.teleport master @a ~ ~ ~ 1 0.4 0

execute at @e[type=snowball,nbt={Item:{tag:{Tags:["wave_distort"]}}}] run scoreboard players operation @p cooldownDisplay = @p wave_distort_cooldown
execute at @e[type=snowball,nbt={Item:{tag:{Tags:["wave_distort"]}}}] run scoreboard players operation @p cooldownDisplay /= tickToSecond globals 
execute at @e[type=snowball,nbt={Item:{tag:{Tags:["wave_distort"]}}}] run execute as @p run title @s[scores={wave_distort_cooldown=1..}] actionbar [{"text":"En Cooldown - ","color":"dark_red","bold":true},{"score":{"objective":"cooldownDisplay","name":"@s"}},{"text":"s","color":"dark_red","bold":true}]

execute at @e[type=snowball,nbt={Item:{tag:{Tags:["wave_distort"]}}}] run execute as @p run execute as @s[scores={wave_distort_cooldown=0}] run scoreboard players set @s wave_distort_cooldown 1200
execute at @e[type=snowball,nbt={Item:{tag:{Tags:["wave_distort"]}}}] run give @p snowball{CustomModelData:4,Tags:["wave_distort"],display:{Name:'[{"text":"Onda de Distorsion","italic":false,"bold":true,"color":"aqua"}]',Lore:['[{"text":"","italic":false}]','[{"text":"ITEM CONSUMIBLE","italic":false,"color":"gold"}]']},Enchantments:[{}]}
scoreboard players remove @a[scores={wave_distort_cooldown=1..}] wave_distort_cooldown 1

kill @e[type=snowball,nbt={Item:{tag:{Tags:["wave_distort"]}}}]