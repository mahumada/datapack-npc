# Reset Trigger
advancement revoke @s from npc:villager_unknown

# Dialogues
scoreboard players set @s[tag=!villager_unknown_interacted] interaction_villager_unknown 0
execute as @s[scores={interaction_villager_unknown=4}] run function npc:interaction/villager_unknown/dialogues/4
execute as @s[scores={interaction_villager_unknown=3}] run function npc:interaction/villager_unknown/dialogues/3
execute as @s[scores={interaction_villager_unknown=2}] run function npc:interaction/villager_unknown/dialogues/2
execute as @s[scores={interaction_villager_unknown=1}] run function npc:interaction/villager_unknown/dialogues/1
execute as @s[scores={interaction_villager_unknown=0}] run function npc:interaction/villager_unknown/dialogues/0

# Add Tag
tag @s add villager_unknown_interacted