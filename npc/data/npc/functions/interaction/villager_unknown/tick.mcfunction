execute as @a[scores={cmdTriggerUnknown=1}] run function npc:interaction/villager_unknown/dialogues/options/1/accept
execute as @a[scores={cmdTriggerUnknown=2}] run function npc:interaction/villager_unknown/dialogues/options/1/decline
execute as @a[scores={cmdTriggerUnknown=3}] run function npc:interaction/villager_unknown/dialogues/options/2/accept
execute as @a[scores={cmdTriggerUnknown=4}] run function npc:interaction/villager_unknown/dialogues/options/2/decline
execute as @a[scores={cmdTriggerUnknown=5}] run function npc:interaction/villager_unknown/dialogues/options/4/accept
execute as @a[scores={cmdTriggerUnknown=6}] run function npc:interaction/villager_unknown/dialogues/options/4/decline
scoreboard players reset @a cmdTriggerUnknown