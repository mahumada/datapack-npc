# Item Count
execute as @s store result score @s item_in_inventory_count run clear @s flint{Tags:["titanite_fragment"]} 0
tag @s[scores={item_in_inventory_count=24..},nbt={SelectedItem:{tag:{Tags:["forge_pickaxe","level_8"]}}}] add payment_ok
# Not Enough
execute as @s[tag=!payment_ok] run tellraw @s ["",{"text":"\n\n\n\n\n"},{"text":"<Kroehler>","bold":true,"underlined":true,"color":"gray"},{"text":"\nSolo puedes mejorar con el pico en la mano principal y los materiales en tu inventario...\n "}]
# Enough
advancement grant @s[tag=payment_ok] only npc:forge/pickaxe_9
execute as @s[tag=payment_ok] run give @s diamond_pickaxe{Tags:["forge_pickaxe","level_9"],display:{Name:'[{"text":"Pico del Guerrero","italic":false,"color":"dark_red","bold":true}]',Lore:['[{"text":"Nivel 9","italic":false,"color":"gray"},{"text":"","italic":false,"color":"dark_purple"}]','[{"text":"","italic":false,"color":"dark_purple"}]','[{"text":"ITEM ESCALABLE","italic":false,"color":"gold"}]']},Unbreakable:1b,Enchantments:[{id:"minecraft:efficiency",lvl:5},{id:"minecraft:fortune",lvl:3}]} 1
execute as @s[tag=payment_ok] run tellraw @s ["",{"text":"\n\n\n\n\n"},{"text":"<Kroehler>","bold":true,"underlined":true,"color":"gray"},{"text":"\nAqui tienes tu pico mejorado, vuelve cuando busques otra mejora...\n "}]
execute as @s[tag=payment_ok] run playsound entity.player.levelup master @s ~ ~ ~ 1 1 0
execute as @s[tag=payment_ok] run clear @s flint{Tags:["titanite_fragment"]} 24
execute as @s[tag=payment_ok] run clear @s diamond_pickaxe{Tags:["forge_pickaxe","level_8"]} 1
tag @s remove payment_ok