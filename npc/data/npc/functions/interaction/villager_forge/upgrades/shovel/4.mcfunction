# Item Count
execute as @s store result score @s item_in_inventory_count run clear @s flint{Tags:["titanite_fragment"]} 0
tag @s[scores={item_in_inventory_count=12..},nbt={SelectedItem:{tag:{Tags:["forge_shovel","level_3"]}}}] add payment_ok
# Not Enough
execute as @s[tag=!payment_ok] run tellraw @s ["",{"text":"\n\n\n\n\n"},{"text":"<Kroehler>","bold":true,"underlined":true,"color":"gray"},{"text":"\nSolo puedes mejorar con la pala en la mano principal y los materiales en tu inventario...\n "}]
# Enough
advancement grant @s[tag=payment_ok] only npc:forge/shovel_4
execute as @s[tag=payment_ok] run give @s diamond_shovel{Tags:["forge_shovel","level_4"],display:{Name:'[{"text":"Pala del Guerrero","italic":false,"color":"dark_red","bold":true}]',Lore:['[{"text":"Nivel 4","italic":false,"color":"gray"},{"text":"","italic":false,"color":"dark_purple"}]','[{"text":"","italic":false,"color":"dark_purple"}]','[{"text":"ITEM ESCALABLE","italic":false,"color":"gold"}]']},Unbreakable:1b,Enchantments:[{id:"minecraft:efficiency",lvl:3}]} 1
execute as @s[tag=payment_ok] run tellraw @s ["",{"text":"\n\n\n\n\n"},{"text":"<Kroehler>","bold":true,"underlined":true,"color":"gray"},{"text":"\nAqui tienes tu pala mejorada, vuelve cuando busques otra mejora...\n "}]
execute as @s[tag=payment_ok] run playsound entity.player.levelup master @s ~ ~ ~ 1 1 0
execute as @s[tag=payment_ok] run clear @s flint{Tags:["titanite_fragment"]} 12
execute as @s[tag=payment_ok] run clear @s diamond_shovel{Tags:["forge_shovel","level_3"]} 1
tag @s remove payment_ok