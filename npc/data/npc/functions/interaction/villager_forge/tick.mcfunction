execute as @a[scores={cmdTriggerForge=2}] run function npc:interaction/villager_forge/upgrades/pickaxe/2
execute as @a[scores={cmdTriggerForge=3}] run function npc:interaction/villager_forge/upgrades/pickaxe/3
execute as @a[scores={cmdTriggerForge=4}] run function npc:interaction/villager_forge/upgrades/pickaxe/4
execute as @a[scores={cmdTriggerForge=5}] run function npc:interaction/villager_forge/upgrades/pickaxe/5
execute as @a[scores={cmdTriggerForge=6}] run function npc:interaction/villager_forge/upgrades/pickaxe/6
execute as @a[scores={cmdTriggerForge=7}] run function npc:interaction/villager_forge/upgrades/pickaxe/7
execute as @a[scores={cmdTriggerForge=8}] run function npc:interaction/villager_forge/upgrades/pickaxe/8
execute as @a[scores={cmdTriggerForge=9}] run function npc:interaction/villager_forge/upgrades/pickaxe/9
execute as @a[scores={cmdTriggerForge=10}] run function npc:interaction/villager_forge/upgrades/pickaxe/10

execute as @a[scores={cmdTriggerForge=12}] run function npc:interaction/villager_forge/upgrades/sword/2
execute as @a[scores={cmdTriggerForge=13}] run function npc:interaction/villager_forge/upgrades/sword/3
execute as @a[scores={cmdTriggerForge=14}] run function npc:interaction/villager_forge/upgrades/sword/4
execute as @a[scores={cmdTriggerForge=15}] run function npc:interaction/villager_forge/upgrades/sword/5
execute as @a[scores={cmdTriggerForge=16}] run function npc:interaction/villager_forge/upgrades/sword/6
execute as @a[scores={cmdTriggerForge=17}] run function npc:interaction/villager_forge/upgrades/sword/7
execute as @a[scores={cmdTriggerForge=18}] run function npc:interaction/villager_forge/upgrades/sword/8
execute as @a[scores={cmdTriggerForge=19}] run function npc:interaction/villager_forge/upgrades/sword/9
execute as @a[scores={cmdTriggerForge=20}] run function npc:interaction/villager_forge/upgrades/sword/10

execute as @a[scores={cmdTriggerForge=32}] run function npc:interaction/villager_forge/upgrades/axe/2
execute as @a[scores={cmdTriggerForge=33}] run function npc:interaction/villager_forge/upgrades/axe/3
execute as @a[scores={cmdTriggerForge=34}] run function npc:interaction/villager_forge/upgrades/axe/4
execute as @a[scores={cmdTriggerForge=35}] run function npc:interaction/villager_forge/upgrades/axe/5

execute as @a[scores={cmdTriggerForge=42}] run function npc:interaction/villager_forge/upgrades/shovel/2
execute as @a[scores={cmdTriggerForge=43}] run function npc:interaction/villager_forge/upgrades/shovel/3
execute as @a[scores={cmdTriggerForge=44}] run function npc:interaction/villager_forge/upgrades/shovel/4
execute as @a[scores={cmdTriggerForge=45}] run function npc:interaction/villager_forge/upgrades/shovel/5

scoreboard players reset @a cmdTriggerForge