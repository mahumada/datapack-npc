tellraw @a {"text":"NPC Dialogue Datapack Loaded Successfully","italic":true,"color":"gray"}

# Scoreboards setup
scoreboard objectives add globals dummy
scoreboard players set tickToSecond globals 20
scoreboard objectives add cooldownDisplay dummy
scoreboard objectives add player_mined_stone minecraft.mined:stone
scoreboard objectives add player_mined_deepslate minecraft.mined:deepslate
scoreboard objectives add player_mined_stone_count dummy
scoreboard players set currentTick globals 0
scoreboard objectives add cmdTriggerForge trigger
scoreboard objectives add cmdTriggerMonk trigger
scoreboard objectives add cmdTriggerUnknown trigger
scoreboard objectives add cmdTriggerArmory trigger

# Init interactions
function npc:interaction/villager_monk/init
function npc:interaction/villager_unknown/init
function npc:interaction/villager_forge/init
function npc:interaction/villager_armory/init
function npc:interaction/villager_lab/init
function npc:interaction/villager_summoner/init

# Gamerules
gamerule sendCommandFeedback false